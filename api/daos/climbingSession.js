const mongoose = require(`mongoose`);
const Schema = mongoose.Schema;
const _ = require('lodash');

let climbingSessionSchema = new Schema(
    {
        creationDate: { type: Schema.Types.Date, default: Date.now },
        date: { type: Schema.Types.Date, required: true },
        duration: { type: Number, required: true },
        grades: { type: String, required: true },
        comment: String,
        userId: { type: String, required: true }
    }
);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Static functions
///
/**
 * Generic find query for a user.
 * @param {string} userId
 * @param {object} [query]
 * @param {boolean} [findOne]
 * @returns {Promise<Category[]> | Promise<Category>}
 * @alias ClimbingSession.findForUser
 */
climbingSessionSchema.statics.findForUser = async function(userId, query = {}, findOne = false) {
    if (findOne) {
        return this.model('climbingSession').findOne(
            _.extend(query, {
                userId: userId,
            })
        );
    } else {
        return this.model('climbingSession').find(
            _.extend(query, {
                userId: userId,
            })
        );
    }
};

module.exports = climbingSessionSchema;
