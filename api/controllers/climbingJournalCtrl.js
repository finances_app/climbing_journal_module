const express = require('express');
const mongoose = require('mongoose');
const _ = require('lodash');

const ClimbingSessionSchema = require('../daos/climbingSession');
const ClimbingSession = mongoose.model('climbingSession', ClimbingSessionSchema);
const ClimbingSessionDto = require('../dtos/climbingSession');

class ClimbingJournalCtrl {
    static getRouter() {
        const router = express.Router({});

        router
            .route('/')
            .get(this._getClimbingSessions)
            .post(this._createClimbingSession);
        router.route('/:session_id').delete(this._deleteClimbingSession);

        return router;
    }

    static async _getClimbingSessions(req, res) {
        let sessions = await ClimbingSession.findForUser(req.userId, {});
        sessions = _.sortBy(sessions, s => new Date(s.date).getTime());
        sessions.reverse();
        return res.send({
            sessions: sessions.map(s => new ClimbingSessionDto(s)),
        });
    }

    static async _createClimbingSession(req, res) {
        try {
            let climbingSession = new ClimbingSession(_.extend(req.body, { userId: req.userId }));
            await climbingSession.save();
            return res.send({});
        } catch (err) {
            return this._handleServerError(req, res, 'Could not save the new climbing session.', err);
        }
    }

    static async _deleteClimbingSession(req, res) {
        let sessionId = req.params.session_id;

        if (!sessionId) {
            // errorHandler.handle500(req, res, new Error('Climbing session id must be provided.'));
            // return;
            return this._handleServerError(req, res, 'Climbing session id must be provided.', new Error());
        }

        try {
            let session = await ClimbingSession.findOne({ _id: sessionId });
            if (!session)
                // return errorHandler.handle500(
                //     req,
                //     res,
                //     new Error(`Error deleting climbing session. No session with id ${sessionId}.`)
                // );
                return this._handleServerError(req, res, `Error deleting climbing session. No session with id ${sessionId}.`, new Error());
            if (session.userId !== req.userId) {
                // errorHandler.handle401(req, res);
                // return;
                return res.status(401).send({
                    url: req.originalUrl,
                    status: 401,
                    error: 'Accès non authorisé à la ressource.',
                });
            }

            await session.remove();
            return res.send({});
        } catch (err) {
            return this._handleServerError(req, res, 'Could not delete the climbing session.', err);
        }
    }

    static _handleServerError(req, res, message, err) {
        res.status(500).send({
            url: req.originalUrl,
            message: message,
            internalError: err
        });
    }
}

module.exports = ClimbingJournalCtrl;
