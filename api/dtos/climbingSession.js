/**
 * Created by hugo on 2019-10-14.
 */

class ClimbingSession {
    constructor({ id, creationDate, date, duration, grades, comment }) {
        this.id = id;
        this.creationDate = creationDate;
        this.date = date;
        this.duration = duration;
        this.grades = grades;
        this.comment = comment;

        Object.seal(this);
    }
}

module.exports = ClimbingSession;
